package com.oreillyauto.util;

import java.nio.charset.Charset;
import java.util.Collections;

import org.apache.tomcat.util.codec.binary.Base64;
//import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class TwilioUtil {
	private static final String MY_NUMBER = "+14173175204";
	private static final String ACCOUNT_SID = "ACe78870b0efe02c543d355c03bc4c6abf";
    private static final String AUTH_TOKEN = "b23396888647904c6a734b12b3742b36";
    
	public TwilioUtil() {}
	
	public void sendSms(String cellNumber, String message) {
		try {
			String url = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json";
	    	// Setup authentication and encode it
	    	String auth = ACCOUNT_SID + ":" + AUTH_TOKEN;
	    	byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
	    	// Create Request Headers
	    	HttpHeaders headers = new HttpHeaders();
	    	headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    	headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	    	headers.set("Authorization", "Basic " + new String(encodedAuth));
	    	// Create Request Body (Payload)
	    	MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
	    	params.add("To", cellNumber);
	    	params.add("From", MY_NUMBER);
	    	params.add("Body", message);
	    	// Send The Request to the Web Service and Print the Response
	    	HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
	    	RestTemplate restTemplate = new RestTemplate();
	    	ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
	    	System.out.println(response.getBody());	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public static void main(String[] args) {
//		new TwilioUtil().sendSms("+14174239219", "Tim is awesome. 92");
//	}
}

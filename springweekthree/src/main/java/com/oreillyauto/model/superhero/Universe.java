package com.oreillyauto.model.superhero;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Universe {
    public final String name;
    public final Team team;

    @JsonCreator
    public Universe(@JsonProperty("name") String name, @JsonProperty("team") Team team){
        this.name = name;
        this.team = team;
    }
}
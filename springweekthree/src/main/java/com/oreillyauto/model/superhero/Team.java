package com.oreillyauto.model.superhero;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Team {
    public final String teamName;
    public final Superhero superheroes[];

    @JsonCreator
    public Team(@JsonProperty("teamName") String teamName, @JsonProperty("superheroes") Superhero[] superheroes){
        this.teamName = teamName;
        this.superheroes = superheroes;
    }
}
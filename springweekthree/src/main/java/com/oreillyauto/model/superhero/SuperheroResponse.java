package com.oreillyauto.model.superhero;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class SuperheroResponse {
    public final Universe universe[];

    @JsonCreator
    public SuperheroResponse(@JsonProperty("Universe") Universe[] universe){
        this.universe = universe;
    }
    
}
package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.UserRepository;
import com.oreillyauto.dao.UserRolesRepository;
import com.oreillyauto.domain.User;
import com.oreillyauto.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	UserRolesRepository userRoleRepo;
	
	public UserServiceImpl() {
	}

	@Override
	public List<User> getUsers() {
		return (List<User>) userRepo.findAll();
	}

	@Override
	public User getUserById(Integer userId) {
		// When we call "findById" the object could be null (when the record
		// does not exist in the database. We need to call .get() on the 
		// optional object to "convert" it to an instance of our Domain Entity.
		Optional<User> optionalUser = userRepo.findById(userId);
		User user = optionalUser.get();
		return user;
	}

	@Override
	@Transactional
	public void saveUser(User user) {
		boolean isNewUser = (user != null && user.getUserId() == null);
		User result = user = userRepo.save(user);
		
		if (isNewUser) {
			System.out.println("Created a new user. User ID = " + result.getUserId());
		}
	}

	@Override
	@Transactional
	public void deleteUser(Integer userId) {
		userRoleRepo.deleteByUserId(userId);
		userRepo.deleteById(userId);
	}

}

package com.oreillyauto.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.EmployeeRepository;
import com.oreillyauto.domain.examples.Employee;
import com.oreillyauto.service.EmployeeService;
import com.oreillyauto.util.Helper;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    @Override
    public void runJpaExamples() {
    	/** FIND LIST */
    	// Query for a user that exists using find
//    	runListJpaExample("find", "Bob", "Smith");
    	
    	// Query for a user that does not exist using find
//    	runListJpaExample("find", "Smith", "Robert");
    	
    	/** GET LIST */
    	// Query for a user that exists using get
//    	runListJpaExample("get", "Bob", "Smith");
    	
    	// Query for a user that does not exist using get
//    	runListJpaExample("get", "Robert", "Smith");
    	
    	/** FIND EMPLOYEE */
    	// Query for a single user that exists using find
//    	runEmployeeJpaExample("find", 1);
    	
    	// Query for a user that does not exist using find
//    	runEmployeeJpaExample("find", 10);                 // Exception!!!!!!!!
    	
    	/** GET EMPLOYEE */
    	// Query for a single user that exists using get
//    	runEmployeeJpaExample("get", 1);
    	
    	// Query for a user that does not exist using get
//    	runEmployeeJpaExample("get", 10);                 // Exception!!!!!!!!
    	
    	/** COUNT EMPLOYEES BY LAST NAME */
//    	Long count = employeeRepository.countByLastName("Smith");
//    	System.out.println("Number of users with last name = \"Smith\": " + count);
    	
    	
    	/** READ LIST */
//    	List<Planner> readList = employeeRepository.findByLastNameAndFirstName("Smith", "Bob");
//    	Helper.printList(readList);
    	
    	/** QUERY LIST */
//    	List<Planner> queryList = employeeRepository.findByLastNameAndFirstName("Smith", "Bob");
//    	Helper.printList(queryList);
    	
    	/** BEFORE */
//    	Date date = Date.valueOf("2020-01-01"); // java.sql.Date
//    	List<Planner> beforeList = employeeRepository.findByStartDateBefore(date);
//    	Helper.printList(beforeList);
    	
    	// Let's perform more on-the-fly examples in class ...
    	
    	
    	/** LIKE */
//    	List<Planner> likeList = employeeRepository.findByLastNameLike("%n");
//    	Helper.printList(likeList);
    	
    	/** IN */
    	List<String> empList = new ArrayList<String>();
    	empList.add("Bob");
    	empList.add("Sue");
    	List<Employee> inList = employeeRepository.findByFirstNameIn(empList);
    	Helper.printList(inList);
    	
    }

	public void runEmployeeJpaExample(String strategy, Integer id) {
		Optional<Employee> employee = null;
		
		// Query for a user that exists
		switch (strategy) {
		case "find":
			employee = employeeRepository.findById(id);	
			break;
		case "get" : 
			employee = employeeRepository.getById(id);
			break;
		}
    	
    	if (employee.isPresent()) {
    		System.out.println(employee.get());
    	} else {
    		System.out.println("User with id="+id+" Not Found");
    	}
	}
	
	public void runListJpaExample(String strategy, String firstName, String lastName) {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		// Query for a user that exists
		switch (strategy) {
		case "find":
			employeeList = employeeRepository.findByLastNameAndFirstName(lastName, firstName);	
			break;
		case "get" : 
			employeeList = employeeRepository.getByLastNameAndFirstName(lastName, firstName);
			break;
		}
    	
    	if (Helper.hasItems(employeeList)) {
    		for (Employee employee : employeeList) {
                System.out.println(employee);
            }	
    	} else {
    		System.out.println("User "+firstName+" "+lastName+" Not Found");
    	}
	}
}

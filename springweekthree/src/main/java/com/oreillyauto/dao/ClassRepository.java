package com.oreillyauto.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.ClassRepositoryCustom;
import com.oreillyauto.domain.foo.Clazz;

public interface ClassRepository extends CrudRepository<Clazz, Integer>, ClassRepositoryCustom {
        
}

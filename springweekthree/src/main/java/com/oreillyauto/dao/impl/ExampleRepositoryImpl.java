package com.oreillyauto.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.examples.Example;
import com.oreillyauto.domain.examples.QExample;
import com.oreillyauto.domain.examples.QNickname;
import com.oreillyauto.util.Helper;
import com.querydsl.core.Tuple;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAUpdateClause;

@Repository
public class ExampleRepositoryImpl extends QuerydslRepositorySupport implements ExampleRepositoryCustom {
	private static final String COLEMAN = "Coleman";

	private static final String BETH = "Beth";
	// Add your queries here. Typed or QueryDSL...
	
	private QExample exampleTable = QExample.example;
	private QNickname nicknameTable = QNickname.nickname;
	
    public ExampleRepositoryImpl() {
        super(Example.class);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExamples() {
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
    
    @SuppressWarnings("unchecked")
    public void printExampleTable() {
        QExample exampleTable = QExample.example;
        
        List<Example> list = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        
        for (Object result : list) {
            System.out.println(result);
        }
    }
    
    public void printList(List<?> list) {
        for (Object result : list) {
            System.out.println(result);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExampleById(Integer id) {
        String sql = "SELECT * " + 
                     "  FROM examples " + 
                     " WHERE id = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, id); // NOTICE PARAMETERS ARE "1-BASED" and not "0-based"
        return (List<Example>)query.getResultList();
    }
    
    
    public Example getFirstExample() {
    	QExample exampleTable = QExample.example;
		Example example = (Example) getQuerydsl().createQuery()
                .from(exampleTable)
                .limit(1)
                .fetch();
        return example;
    }
    
    @SuppressWarnings("unchecked")
    public List<Example> getAllExamples() {
    	QExample exampleTable = QExample.example;
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
	
	@Override
	public void testQueries(String day) {
		switch (day) {
		case "3":
			testDayThree();
			break;
		case "4":
			testDayFour();
			break;
		}
	}
    
	@SuppressWarnings("unchecked")
	private void testDayFour() {
		
		// INSERT Example - Typed SQL
		// Since we deleted all users with last_name = "Brannon" yesterday,
		// let's create a new one.
        String sql = "INSERT INTO examples (first_name, last_name) " + 
                     "VALUES ('Jeffery', 'Brannon') ";
        Query query = getEntityManager().createNativeQuery(sql);
        query.executeUpdate();
		
		
/**
	ResultTransformer
	Querydsl provides a meaningful way to customize results using the ResultTransformer for aggregation. 
	The transformer works directly with the group by function to transform the data in memory. When 
	working with group by, realize that we are working with aggregate functions and therefore end up 
	with objects that do not necessarily relate to each other in any way (i.e. they are not necessarily 
	columns we would find in the table we are querying - like AVG). This list of potentially disjointed
	data is a Tuple. 
 */
		
        // Tuple example using a group by 
		// exampleTable.count() is our aggregate
        List<Tuple> list = (List<Tuple>) (Object) getQuerydsl().createQuery()
            .select(exampleTable.lastName, exampleTable.count())
            .from(exampleTable)
            .groupBy(exampleTable.lastName)
            .fetch();
        
        printList(list);
		
		
/**
	Query for Map using the Transform function
	Since we are working with aggregate data, we can transform the data into a java.util.Map, given 
	that the aggregate column can become the key and hold some value.
 */
        // Map - Transform Example - Group By Last Name
        Map<String, Example> exampleMap = getQuerydsl().createQuery()
                .from(exampleTable)
                .where(exampleTable.id.in(
                    JPAExpressions.select(exampleTable.id)
                    .from(exampleTable)
                    ))
                .transform(GroupBy.groupBy(exampleTable.lastName).as(exampleTable));
        
        System.out.println("Map - Example 1");
        Helper.printMap(exampleMap);
        
        
        
        // Map - Transform Example - group by first name, then transform and group by first name
        exampleMap = getQuerydsl().createQuery()
                .from(exampleTable)
                .groupBy(exampleTable.firstName)
                .transform(GroupBy.groupBy(exampleTable.firstName).as(exampleTable));
        
        System.out.println("Map - Example 2");
        Helper.printMap(exampleMap);       
        
        
        
        
        // Map - Transform Example - group by first name, then transform and group by first name
        Map<String, Long> exampleCount = getQuerydsl().createQuery()
                .from(exampleTable)
                .groupBy(exampleTable.firstName)
                .transform(GroupBy.groupBy(exampleTable.firstName).as(exampleTable.count()));
        
        System.out.println("Map - Example 3");
        Helper.printMap(exampleCount);
     
        
        // Boolean Builders!
        Predicate firstAndLastNamePredicate = exampleTable.firstName.eq(BETH)
                .and(exampleTable.lastName.eq(COLEMAN));
        
        Predicate idPredicate = exampleTable.id.eq(3);
        Example example = (Example) getQuerydsl().createQuery()
        		.from(exampleTable)
        		.where(exampleTable.firstName.eq(BETH)
        				.and(firstAndLastNamePredicate)
        				.and(idPredicate)
        				)
        		.limit(1)
        		.fetchOne();

        System.out.println("Boolean Builder - Example 4");
        System.out.println(example);
        
	}	
	
	@SuppressWarnings("unchecked")
	public void testDayThree() {
		// We can cast from List to Object in Java 
     	List<Integer> list = new ArrayList<Integer>();
    	list.add(1);
    	
    	// Integer myInteger = (Integer)list;  Cannot cast from List<Integer> to Integer
    	// Nope
    	
    	// Simple example - retrieve one record 
    	Example example = (Example) getQuerydsl().createQuery()
    	           .from(exampleTable)
    	           .limit(1)
    	           .fetchOne();

        // EXAMPLE 1 -----------------------------------------------------------------
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        //return eventList;
    	
        // EXAMPLE 2 -----------------------------------------------------------------
        // java.persistence.Query takes a SQL string expression "SELECT * FROM <table>";
        String sql = "SELECT * " + 
                     "  FROM examples ";
        Query query = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> blah = (List<Object[]>)query.getResultList();
        for (Object[] b: blah) {
        	System.out.println("Example 2: " + b[1]);
        	System.out.println("Example 2: " + b[2]);
        }
        System.out.println("---------------------");                       
        
        //return (List<Example>)query.getResultList();
        
        // EXAMPLE - Select One "Jeffery"  -------------------------------------------
        Example example77 = (Example) getQuerydsl().createQuery()
                .from(exampleTable)
                .where(exampleTable.firstName.eq("Jeffery"))
                .fetchOne();

        
        // EXAMPLE - Select users with first name = "Jeffery" ------------------------
        List<Example> jefferyList = (List<Example>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.id, exampleTable.firstName, exampleTable.lastName)
                .from(exampleTable)
                .where(exampleTable.firstName.eq("Jeffery"))
                .fetch();
                
        // Example - Multiple sources (tables) ----------------------------------------
        List<Object> exampleList3 = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .innerJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .fetch();
                
        // Example - Multiple Filters -------------------------------------------------
        List<Object> exampleList4 = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .innerJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .where(nicknameTable.nickName.eq("Giraffe"), (nicknameTable.nickName.eq("Nate Dawg")))
                .fetch();  // Comma in the predicate is an "or"
        
        // Example - Multiple Filters --------------------------------------------------
        List<Object> exampleList4a = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .innerJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .where(
                		     nicknameTable.nickName.eq("Giraffe")
                		 .or((nicknameTable.nickName.eq("Nate Dawg")))
                	  ) // another example using or
                
                .fetch();
        
        // Example - Left Join ----------------------------------------------------------
        List<Object> exampleList5 = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .leftJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .fetch();
        
        System.out.println("exampleList5: " + ((Tuple) exampleList5.get(0)).get(exampleTable.lastName));

        // Example - Ordering -----------------------------------------------------------
        List<Example> exampleList6 = (List<Example>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName)
                .from(exampleTable)
                .orderBy(exampleTable.firstName.asc())
                .fetch();
        
        Tuple t = (Tuple) exampleList6.get(0);
        System.out.println(t.get(exampleTable.firstName));
        System.out.println("exampleList6: " + exampleList6.get(0));                              
        
        
        // Example - Grouping -----------------------------------------------------------
        List<Example> exampleList7 = (List<Example>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.lastName)
                .from(exampleTable)
                .groupBy(exampleTable.lastName)
                .fetch();
        
        // Example - Delete -------------------------------------------------------------
        EntityManager em = getEntityManager();
        em.joinTransaction();
        long deletedItemsCount = new JPADeleteClause(em, exampleTable)
                .where(exampleTable.lastName.eq("Brannon"))
                .execute();
        
        System.out.println(deletedItemsCount + " records deleted.");

        
       
        
        
        // Now that we have potentially deleted some records, let's query the entire
        // table and print the results
        List<Example> exampleList8 = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        
        for (Object result : exampleList8) {
            System.out.println(result);
        }

        // Example - Update ------------------------------------------------------------
        long updatedCount = new JPAUpdateClause(getEntityManager(), exampleTable)
                .where(exampleTable.lastName.toLowerCase().eq("Dawg"))
                .set(exampleTable.lastName, "Duck")
                .execute();
//            System.out.println(updatedCount + " records updated.");
//            printExampleTable();

         // Example - Sub Query ---------------------------------------------------------
            Object obj = getQuerydsl().createQuery()
                    .from(exampleTable)
                    .where(exampleTable.id.in(
                        JPAExpressions.select(exampleTable.id.max())
                        .from(exampleTable)
                        ))
                    .fetchOne();    
            
            System.out.println("obj=> "  + obj);
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getTables() {
        String sql = "  SELECT st.tablename " + 
        	  	     "    FROM sys.systables st " + 
        		     "   WHERE st.tablename NOT LIKE 'SYS%'" +
        		     "ORDER BY st.tablename";
        Query query = getEntityManager().createNativeQuery(sql);
        List<Object> result =  (List<Object>)query.getResultList();        
        return result;
    }
}


package com.oreillyauto.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.UniversityRepositoryCustom;
import com.oreillyauto.domain.university.Course;
import com.oreillyauto.domain.university.QCourse;
import com.oreillyauto.domain.university.QUniversity;
import com.oreillyauto.domain.university.University;
import com.oreillyauto.util.Helper;
import com.querydsl.core.group.GroupBy;

@Repository
public class UniversityRepositoryImpl extends QuerydslRepositorySupport implements UniversityRepositoryCustom {
	
	QCourse coursesTable = QCourse.course;
	QUniversity universityTable = QUniversity.university;
	
	
    public UniversityRepositoryImpl() {
        super(University.class);
    }

	@Override
	public void testUniversityQueries() {
		
		
		/** Example 1 */
		// Get a Map of Courses grouped by course name
		// The key is the course name and the value is the course object
		Map<String, Course> resultMap = 
				from(coursesTable)
                .orderBy(coursesTable.courseName.asc())                
                .transform(GroupBy.groupBy(coursesTable.courseName) // Map Key
                		.as(coursesTable));                         // Map Value
		
		for (Map.Entry<String, Course> entry : resultMap.entrySet()) {
			System.out.println("Key = " + entry.getKey() + 
                    ", Value = " + entry.getValue());
		}
		 
      
		
		
		/** Example 2 */
		// Get a Map of Courses grouped by course name
		// The key is the course name and the value is the Universities that have the course
		Map<String, List<University>> resultMap2 =  
				from(coursesTable)
	            .innerJoin(universityTable)
	            .on(coursesTable.courseId.eq(universityTable.course.courseId))
	            .orderBy(coursesTable.courseName.asc(), universityTable.universityName.asc())
	            .transform(GroupBy.groupBy(coursesTable.courseName)  //Map Key
	            	.as(GroupBy.list(universityTable)));             //Map Value
		
		for (Map.Entry<String, List<University>> entry : resultMap2.entrySet()) {
			System.out.println("Key = " + entry.getKey() + 
                    ", Value = " + entry.getValue());
		}

		
		
		/** Example 3 */
		// Team Example: Build a QueryDSL Example as a team  
		
		
		
		
	}
    
}


package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.StudentRepositoryCustom;
import com.oreillyauto.domain.schools.Student;

public interface StudentRepository extends CrudRepository<Student, Integer>, StudentRepositoryCustom {
    
}

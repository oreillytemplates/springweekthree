package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.UserRolesRepositoryCustom;
import com.oreillyauto.domain.examples.UserRole;

public interface UserRolesRepository extends CrudRepository<UserRole, Integer>, UserRolesRepositoryCustom {

	
      
    // Add your interface methods here (that ARE Spring Data methods)
    
}

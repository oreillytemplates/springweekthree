package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.FacilityRepositoryCustom;
import com.oreillyauto.domain.facilities.Facility;

public interface FacilityRepository extends CrudRepository<Facility, Integer>, FacilityRepositoryCustom {
    
}

package com.oreillyauto.domain.university;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "course")
public class Course implements Serializable {
    private static final long serialVersionUID = -8221336284781448020L;
    
//    course_guid INTEGER NOT NULL AUTO_INCREMENT,
//    course_name VARCHAR(100),
//    credit INTEGER,
//	   professor_id INTEGER,
//	   room_id INTEGER,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_guid")
    private Integer courseId;
    
    @Column(name = "course_name")
    private String courseName;
    
    @Column(name = "credit")
    private String credit; 
    
    @Column(name = "professor_id")
    private String professorId; 
    
    @Column(name = "room_id")
    private Integer roomId; 
    
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "course")
    private List<University> universityList;

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public List<University> getUniversityList() {
		return universityList;
	}

	public void setUniversityList(List<University> universityList) {
		this.universityList = universityList;
	}
		

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getProfessorId() {
		return professorId;
	}

	public void setProfessorId(String professorId) {
		this.professorId = professorId;
	}

	@Override
	public String toString() {
		return "Course [courseGuid=" + courseId + ", courseName=" + courseName + "]";
	}

}

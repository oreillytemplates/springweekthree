package com.oreillyauto.controllers.superheroes;

import java.nio.charset.Charset;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.oreillyauto.model.superhero.SuperheroResponse;

@Controller
public class SuperheroClientController {

	@ResponseBody
	@GetMapping(value = { "/superhero/testSuperherov1" })
	public String getTestAuthenticatedRestServiceV1(HttpServletRequest req) {

		String url = "http://localhost:8080" + req.getContextPath() + "/rest/superheroes/v1";

		// Setup authentication and encode it
		String auth = "cGFzc3dvcmQ=";
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));

		// Create Request Headers
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("Authorization", "Basic " + new String(encodedAuth));

		// Create Request Body (Payload) (if applicable)
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

		// Send The Request to the Web Service and Print the Response
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
		RestTemplate restTemplate = new RestTemplate();

		// Get the string representation of the JSON
		// ResponseEntity<String> response = restTemplate.postForEntity(url, entity,
		// String.class);
		// System.out.println(response.getBody());

		// Get the List<Intern> representation of the JSON
		ResponseEntity<SuperheroResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity,
				SuperheroResponse.class);
		SuperheroResponse superheroResponse = responseEntity.getBody();

		// Print Response
		System.out.println(new Gson().toJson(superheroResponse));
		// System.out.println(superheroResponse);

		return "done"; 
//    	return new Gson().toJson(superheroResponse);
	}

}

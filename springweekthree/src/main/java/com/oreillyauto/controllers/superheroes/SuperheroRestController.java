package com.oreillyauto.controllers.superheroes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.oreillyauto.model.superhero.Superhero;
import com.oreillyauto.model.superhero.SuperheroResponse;
import com.oreillyauto.model.superhero.Team;
import com.oreillyauto.model.superhero.Universe;
import com.oreillyauto.util.RestHelper;

import io.jsonwebtoken.lang.Collections;

@RestController
public class SuperheroRestController {
	
//    @GetMapping(value = "/rest/interns", produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Intern> getAllInterns() {
//    	return mappingService.getInterns();
//    }
    
    @PostMapping(value = "/rest/superheroes/v1", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAllSuperheroesV1(@RequestHeader(value="Authorization") String authString) { 
    	Superhero[] superheroes = (Superhero[])Arrays.asList(new Superhero[] {new Superhero("Iron Man"), 
    			new Superhero("Hulk"), new Superhero("Black Widow"), new Superhero("Thor")}).toArray();
    	Team team = new Team("Avengers", superheroes);
    	Universe universeEntry = new Universe("Marvel", team);
    	Universe[] universe = new Universe[1];
    	universe[0] = universeEntry;
    	return new SuperheroResponse(universe);
    }
    
	
//	// Iron Man
//	JsonObject im = new JsonObject();
//	im.addProperty("name", "Iron Man");
//	// Hulk
//	JsonObject hulk = new JsonObject();
//	hulk.addProperty("name", "Hulk");
//	// Black Widow
//	JsonObject bw = new JsonObject();
//	bw.addProperty("name", "Hulk");
//	// Black Widow
//	JsonObject thor = new JsonObject();
//	thor.addProperty("name", "Thor");
//	
//	// Build Superheroes array
//	JsonArray superheroes = new JsonArray();
//	superheroes.add(im);
//	superheroes.add(hulk);
//	superheroes.add(bw);
//	superheroes.add(thor);
//	
//	// Build Team
//	JsonObject team = new JsonObject();
//	team.addProperty("teamName", "Avengers");
//	team.add("superheroes", superheroes);
//	   	
//	// Build Universe Object
//	JsonObject universeEntry = new JsonObject();
//	universeEntry.addProperty("name", "Marvel");
//	universeEntry.add("team", team);
//	
//	// Build Universe Array
//	JsonArray universeArray = new JsonArray();
//	universeArray.add(universeEntry);
//	
//	JsonObject response = new JsonObject();
//	response.add("Universe", universeArray);
//	//System.out.println(response.toString());
//	return response;
	
	
	
	
//    if (!RestHelper.isUserAuthenticated(authString)){
//        return "{\"error\":\"User not authenticated\"}";
//    }
//    
//    // Get Superhero Array
//    List<Superhero> shList = new ArrayList<Superhero>();
//    shList.add(new Superhero("Iron Man"));
//    shList.add(new Superhero("Hulk"));
//    shList.add(new Superhero("Black Widow"));
//    shList.add(new Superhero("Thor"));        
//    Superhero[] superheroes = shList.toArray(new Superhero[shList.size()]);
//    
//    // Get Team
//    Team team = new Team("Avengers", superheroes);
//    
//    // Get Universe 
//    Universe univers = new Universe("Marvel", team);
//    
//    // Add Universe to array of Universe
//    Universe[] universe = new Universe[1];
//    universe[0] = univers;
//    
//    
//    
//    //Gson gson = new Gson();
//    //return "{    \"Universe\" : [        {            \"name\" : \"Marvel\",            \"team\" : {                    \"teamName\"  \"Avengers\",                    \"superheroes\" : [{\"name\" : \"Iron Man\"},                            {\"name\" : \"Hulk\"},                            {\"name\" : \"Black Widow\"},                            {\"name\" : \"Thor\"}]               }        }]}";
//    //return null;
//    
//    System.out.println(new Gson().toJson(universe));
//    
//    return universe;
    
//    public static void main(String[] args) {
//    	
//    	// Iron Man
//    	JsonObject im = new JsonObject();
//    	im.addProperty("name", "Iron Man");
//    	// Hulk
//    	JsonObject hulk = new JsonObject();
//    	hulk.addProperty("name", "Hulk");
//    	// Black Widow
//    	JsonObject bw = new JsonObject();
//    	bw.addProperty("name", "Hulk");
//    	// Black Widow
//    	JsonObject thor = new JsonObject();
//    	thor.addProperty("name", "Thor");
//    	
//    	// Build Superheroes array
//    	JsonArray superheroes = new JsonArray();
//    	superheroes.add(im);
//    	superheroes.add(hulk);
//    	superheroes.add(bw);
//    	superheroes.add(thor);
//    	
//    	// Build Team
//    	JsonObject team = new JsonObject();
//    	team.addProperty("teamName", "Avengers");
//    	team.add("superheroes", superheroes);
//    	   	
//    	// Build Universe Object
//    	JsonObject universeEntry = new JsonObject();
//    	universeEntry.addProperty("name", "Marvel");
//    	universeEntry.add("team", team);
//    	
//    	// Build Universe Array
//    	JsonArray universeArray = new JsonArray();
//    	universeArray.add(universeEntry);
//    	
//    	JsonObject response = new JsonObject();
//    	response.add("Universe", universeArray);
//    	
//    	System.out.println(response.toString());
//    	
//    	
//    	
////        // Get Superhero Array
////        List<Superhero> shList = new ArrayList<Superhero>();
////        shList.add(new Superhero("Iron Man"));
////        shList.add(new Superhero("Hulk"));
////        shList.add(new Superhero("Black Widow"));
////        shList.add(new Superhero("Thor"));        
////        Superhero[] superheroes = shList.toArray(new Superhero[shList.size()]);
////        
////        // Get Team
////        Team team = new Team("Avengers", superheroes);
////        
////        // Get Universe 
////        Universe univers = new Universe("Marvel", team);
////        
////        // Create universe array and then add universe to the array
////        Universe[] universe = new Universe[1];
////        universe[0] = univers;   	
////    	
////        System.out.println(new Gson().toJson(universe));
//    }
    
}

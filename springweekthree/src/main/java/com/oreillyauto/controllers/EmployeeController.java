package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.oreillyauto.service.EmployeeService;

@Controller
public class EmployeeController extends BaseController {
    @Autowired
    EmployeeService employeeService;
    
    @GetMapping(value = { "/employees" })
    public String getEmployees() {
    	employeeService.runJpaExamples();
        return "employees";
    }    
}

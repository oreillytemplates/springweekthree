package com.oreillyauto.controllers;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
//import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreillyauto.domain.examples.Example;
import com.oreillyauto.domain.groupkt.Response;
import com.oreillyauto.domain.groupkt.RestResponse;
import com.oreillyauto.domain.groupkt.Result;
import com.oreillyauto.domain.gturnquist.Quote;
import com.oreillyauto.domain.gturnquist.Value;
import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.service.ExampleService;
import com.oreillyauto.vo.InternListVO;

@Controller
public class ExampleController {
	private static final String SPRING_BOOT = "Spring Boot";
	private Map<Long, Quote> msgMap = new HashMap<Long, Quote>();

	@Autowired
	ExampleService exampleService;

	@PostConstruct
	private void init() {
		initializeSpringMessages();
	}

	@ResponseBody
	@GetMapping(value = { "/example/testInternRestv1" })
	public String getTestAuthenticatedRestServiceV1(HttpServletRequest req) {

		String url = "http://localhost:8080" + req.getContextPath() + "/rest/interns/v1";

		// Setup authentication and encode it
		String auth = "cGFzc3dvcmQ=";
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));

		// Create Request Headers
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.set("Authorization", "Basic " + new String(encodedAuth));

		// Create Request Body (Payload) (if applicable)
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

		// Send The Request to the Web Service and Print the Response
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
		RestTemplate restTemplate = new RestTemplate();

		// Get the string representation of the JSON
		// ResponseEntity<String> response = restTemplate.postForEntity(url, entity,
		// String.class);
		// System.out.println(response.getBody());

		// Get the List<Intern> representation of the JSON
		ResponseEntity<List<Intern>> rateResponse = restTemplate.exchange(url, HttpMethod.POST, entity,
				new ParameterizedTypeReference<List<Intern>>() {
				});
		List<Intern> internList = rateResponse.getBody();

		for (Intern intern : internList) {
			System.out.println(intern);
		}

		return "done";
	}

	@ResponseBody
	@GetMapping(value = { "/example/testInternRestTemplate" })
	public String getTestInternsTemplateRestService(HttpServletRequest req, Model model)
			throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		String serviceUri = "http://localhost:8080" + req.getContextPath() + "/rest/internsTemplate";

		ResponseEntity<InternListVO> response = restTemplate.exchange(serviceUri, HttpMethod.GET, null,
				new ParameterizedTypeReference<InternListVO>() {
				});
		InternListVO internListVO = response.getBody();

		for (Intern intern : internListVO.getInternList()) {
			System.out.println(intern);
		}

		return new RestTemplate().getForObject(serviceUri, String.class);
	}

	@ResponseBody
	@GetMapping(value = { "/example/testInternRest" })
	public String getTestInternsRestService(HttpServletRequest req, Model model) throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		String serviceUri = "http://localhost:8080" + req.getContextPath() + "/rest/interns";

		ResponseEntity<List<Intern>> response = restTemplate.exchange(serviceUri, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Intern>>() {
				});
		List<Intern> internList = response.getBody();

		for (Intern intern : internList) {
			System.out.println(intern);
		}

		System.out.println(restTemplate.getForEntity(serviceUri, String.class).getBody());
		return restTemplate.getForEntity(serviceUri, String.class).getBody();
	}

	@GetMapping(value = { "/example/restclient" })
	public String getTestSpringBoot(Model model) throws JsonProcessingException {
		// Declare the service, service uri, and build the Rest Template
		String service = SPRING_BOOT;
		//String serviceUri = "https://gturnquist-quoters.cfapps.io/api/random";
		String serviceUri = "http://localhost:8080/example/springmessages";

		// Use a RestTemplate to communicate/handle the web request
		RestTemplate restTemplate = new RestTemplate();

		// Call the service and transform the JSON response into a Quote Object
		Quote quote = restTemplate.getForObject(serviceUri, Quote.class);

		// Format the response, add the details to the model and return the JSP
		ObjectMapper mapper = new ObjectMapper();
		String response = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(quote);

		model.addAttribute("service", service);
		model.addAttribute("serviceUri", serviceUri);
		model.addAttribute("response", response);

		//model.addAttribute("response", quote.toString());

		return "restclient";
	}

	@GetMapping(value = { "/example/testgroupkt/{countryCode}" })
	public String getTestGroupKt(HttpServletRequest req, @PathVariable String countryCode, Model model)
			throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		String service = "Group Kt";

		// If the API is NOT working use this
		// String serviceUri = "http://localhost:8080" + req.getContextPath() +
		// "/example/getgroupkt";

		// If the API is working use this
		String serviceUri = "http://www.groupkt.com/country/get/iso2code/" + countryCode;

		// System.out.println("Processing:\n" + restTemplate.getForObject(serviceUri,
		// String.class));
		Response response = restTemplate.getForObject(serviceUri, Response.class);
		ObjectMapper mapper = new ObjectMapper();
		String responseJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);
		model.addAttribute("service", service);
		model.addAttribute("serviceUri", serviceUri);
		model.addAttribute("response", responseJson);
		return "restclient";
	}

	@ResponseBody
	@GetMapping(value = { "/example/getgroupkt" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, RestResponse> getgroupkt() {
		RestResponse rest = new RestResponse();
		rest.setMessages(new String[] { "Country found matching code [IN]." });
		rest.setResult(new Result("India", "IN", "IND"));
		Map<String, RestResponse> map = new HashMap<String, RestResponse>();
		map.put("RestResponse", rest);
		return map;
	}

	@GetMapping(value = { "/example" })
	public String login(Model model) throws Exception {
		List<Example> exampleList = exampleService.getExamples();
		ObjectMapper mapper = new ObjectMapper();
		String result = mapper.writeValueAsString(exampleList);
		model.addAttribute("result", result);
		return "example";
	}

	@GetMapping(value = { "/example/test/{day}" }) // 3 or 4
	public String getTest(@PathVariable String day, Model model) throws Exception {
		exampleService.testQueries(day);
		return "example";
	}

	@ResponseBody
	@GetMapping(value = { "/example/test/mediatype" }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String getTestMediatype() {
		return "test";
	}

	/**********/
	@ResponseBody
	@GetMapping(value = "/example/springmessages")
	public Quote getSpringMessages() {
		int random = (int) (Math.random() * 10);
		return msgMap.get(Long.valueOf(random));
	}

	private void initializeSpringMessages() {
		msgMap = new HashMap<Long, Quote>();
		String[] messages = new String[10];
		messages[0] = "Spring Boot is awesome!";
		messages[1] = "Spring Boot is so fast";
		messages[2] = "Spring Boot is easy to use";
		messages[3] = "Spring Boot can manage dependencies well";
		messages[4] = "Spring Boot takes shorter time to load than Spring";
		messages[5] = "Spring Boot Boot Boot Boot";
		messages[6] = "Start using Spring Boot for your web development";
		messages[7] = "Try Spring Boot, you will love it";
		messages[8] = "It is easy to deploy Spring Boot application";
		messages[9] = "Really loving Spring Boot, makes stand alone Spring apps easy";

		for (int i = 0; i < messages.length; i++) {
			msgMap.put(Long.valueOf(i), getQuote(Long.valueOf(i), messages[i]));
		}
	}

	private Quote getQuote(Long id, String message) {
		Quote quote = new Quote();
		quote.setType("success");
		Value value = new Value();
		value.setId(id);
		value.setQuote(message);
		quote.setValue(value);
		return quote;
	}

}

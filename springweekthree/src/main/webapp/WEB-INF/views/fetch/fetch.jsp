<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Fetch Examples</h1>

<!-- ---------------------------- EXAMPLE 1 ---------------------------- -->
<h4>Example 1</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console1">
reset('output1');
var promise1 = new Promise(function(resolve, reject) {
	setTimeout(function() {
		resolve('foo');
	}, 300);
	printResponse("bar", 'output1');
});

promise1.then(function(value) {
	printResponse(value, 'output1');
});

printResponse(promise1, 'output1');
</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console1', 'output1');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output1"></div>
	</div>
</div>
<hr/>
<!-- ---------------------------- EXAMPLE 2 ---------------------------- -->
<h4>Example 2</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console2">
reset('output2');

var promise1 = new Promise(function(resolve, reject) {
	  reject('server down!');
});

promise1.then(function(value) {
	printResponse(("Resolved: " + value), 'output2');
})
.catch(function(error) {
	printResponse(("Rejected: " + error), 'output2');
});
</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console2', 'output2');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output2"></div>
	</div>
</div>

<hr/>
<!-- ---------------------------- EXAMPLE 3 ---------------------------- -->

<h4>Example 3</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console3">
reset('output3');

try {
	var promise1 = new Promise(function(resolve, reject) {
		throw new Error("Unknown Exception!")
	});
  
	promise2.then(function(value) {
		printResponse(("Resolved: " + value), 'output3');
	})
	.catch(function(error) {
		printResponse(("Rejected: " + error), 'output3');
	});
} catch (e) {
	printResponse(("Runtime Exception : " + e), 'output3');
}
</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console3', 'output3');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output3"></div>
	</div>
</div>

<hr/>
<!-- ---------------------------- EXAMPLE 4 ---------------------------- -->

<h4>Example 4</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console4">
reset('output4');

fetch(' http://echo.jsontest.com/key/value/one/two')
.then(function(response) {
	
	// Is the result from response.json() a Promise object?
	// Let's assign it to a variable and print it to check...
	let rj = response.json();
	printResponse(rj, 'output4');
	
	// Can we process the Promise immediately in this .then code block?
	let str = JSON.stringify(rj);
	printResponse(str, 'output4');
	
	// Return the Promise to ANOTHER .then for further processing
	return rj;
})
.then(function(myJson) {
	printResponse(JSON.stringify(myJson), 'output4');
})
.catch(function(error) {
	printResponse(("Rejected: " + error), 'output4');
});

</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console4', 'output4');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output4"></div>
	</div>
</div>

<hr/>
<!-- ---------------------------- EXAMPLE 5 ---------------------------- -->

<h4>Example 5</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console5">
reset('output5');

fetch('http://localhost:8080/fetch/404')
.then(function(response) {
	return response.json();
})
.then(function(myJson) {
	printResponse(JSON.stringify(myJson), 'output5');
})
.catch(function(error) {
	printResponse(("Error/Rejected: " + error), 'output5');
});
</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console5', 'output5');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output5"></div>
	</div>
</div>

<hr/>
<!-- ---------------------------- EXAMPLE 6 ---------------------------- -->

<h4>Example 6</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console6">
reset('output6');

fetch('http://localhost:8080/fetch/404')
.then(function(response) {
		
	if (response.ok) {
		return response.json();
	} else {
		printResponse(getError(response), 'output6');
		return;
	}
})
.then(function(myJson) {
	let jsonObject = JSON.parse(myJson);
	printResponse(jsonObject, 'output6');
})
.catch(function(error) {
	printResponse(("Error/Rejected: " + error), 'output6');
});
</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console6', 'output6');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output6"></div>
	</div>
</div>

<hr/>
<!-- ---------------------------- EXAMPLE 7 ---------------------------- -->

<h4>Example 7</h4>
<div class="row example">
	<div class="col-sm-7">
		<label>JavaScript</label>
<textarea placeholder="Add JavaScript Here" id="console7">
reset('output7');

fetch('http://localhost:8080/fetch/404')
.then(function(response) {
		
	if (response.ok) {
		return response.json();
	} else {
		throw new Error(getError(response));
	}
})
.then(function(myJson) {
	let jsonObject = JSON.parse(myJson);
	printResponse(jsonObject, 'output7');
})
.catch(function(error) {
	printResponse(("Error/Rejected: " + error), 'output7');
});
</textarea>
		<div>
			<button class="btn btn-success float-right" onclick="run('console7', 'output7');">
				Run
			</button>
		</div>
	</div>
	<div class="col-sm-5">
		<label>Output</label>
		<div class="bordered" id="output7"></div>
	</div>
</div>

<hr/>
<!-- ---------------------------- EXAMPLE 8 ---------------------------- -->


<!-- --------------------- END OF EXAMPLES ---------------------- -->

<!-- <div id="response"></div> -->
<style>
textarea {
  resize: both;
  overflow: auto;
  width: 100%;
}
.example, .bordered {
	border: thin lightgrey solid;
	min-height: 200px;
	width: 100%;
	padding: 5px;
}
label {
	margin-left: auto;
	margin-right: auto;
	text-align: center !important;
	display: block;
	color: blue;
}
h4 {
	color: #009d57 !important;	
}
hr {
	width: 100%;
	border: 1px solid #0000ff;
}
</style>
<script>
	var responseDiv;
	
	orly.ready.then(()=>{
		adjustTextareas();
	});
	
	function adjustTextareas() {
		var textareaList = document.getElementsByTagName("textarea");
		
		for (let ta of textareaList) {
			textAreaAdjust(ta);
		} 
	}
	
	function textAreaAdjust(ta) {
		ta.style.height = "1px";
		ta.style.height = (10+ta.scrollHeight) + "px";
	}
	
	function runExampleOne() {
		var promise1 = new Promise(function(resolve, reject) {
			setTimeout(function() {
				resolve('foo');
			}, 300);
			printResponse("bar");
		});

		promise1.then(function(value) {
			printResponse(value);
		});

		printResponse(promise1);
	}
	
	function runExampleTwo() {
		var promise1 = new Promise(function(resolve, reject) {
			  reject('server down!');
		});

		promise1.then(function(value) {
			printResponse("Resolved: " + value);
		})
		.catch(function(error) {
			printResponse("Rejected: " + error);
		});
	}
	
	function runExampleThree() {
		try {
			var promise1 = new Promise(function(resolve, reject) {
				throw new Error("Unknown Exception!")
			});
		  
			promise2.then(function(value) {
				printResponse("Resolved: " + value);
			})
			.catch(function(error) {
				printResponse("Rejected: " + error);
			});
		} catch (e) {
			printResponse("Runtime Exception : " + e);
		}
	}
	
	function runExampleFour() {
		
	}
	
	function printResponse(response, outputTarget) {
		let div = orly.qid(outputTarget);
		div.innerHTML += (response + "<br/>");
	}
	
	function reset(target) {
		orly.qid(target).innerHTML = "";
	}
	
	/**
	 * Note that you must create a new element because in HTML5, each 
	 * script element has an associated flag to indicate whether it's 
	 * been executed and it can only be executed once. Replacing the 
	 * content doesn't reset the flag, and a cloned script element keeps 
	 * the setting of the element it was cloned from.
	 */
	function run(input) {
	    var el = orly.qid(input);
	    var scriptText = el.value;
	    var oldScript = orly.qid('scriptContainer');
	    var newScript;

	    if (oldScript) {
	      oldScript.parentNode.removeChild(oldScript);
	    }

	    newScript = document.createElement('script');
	    newScript.id = 'scriptContainer';
	    newScript.text = el.value;
	    document.body.appendChild(newScript);
	} 

	function getError(errorObject) {
		if (errorObject == null || errorObject == "undefined") {
			return "Unknown Exception";
		} else {
			let status = errorObject.status;
			let text = errorObject.statusText;
			console.log(status + " " + text);
			return "Server Error. Status="+status+" Msg="+text;
		}
	}
	
</script>
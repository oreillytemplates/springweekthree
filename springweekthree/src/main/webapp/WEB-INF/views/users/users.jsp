<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>O'Reilly Employees</h1>
<orly-alert-mgr id="myAlertManager"></orly-alert-mgr>
<%-- Load Table Data Directly (After Checking for missing/empty variable) --%>
<c:set value="${fn:length(userListJson) gt 0 ? userListJson : []}" var="tableData"/>
<a class="btn btn-primary" href="<c:url value='/users/addUser'/>">Add User</a>
<orly-table id="userTable" loaddataoncreate includefilter bordered maxrows="10" tabletitle="Search Results" class="invisible" data='${tableData}'>
	<%-- Load Table Data Directly (NOT Checking for missing/empty variable) --%>
	<%-- <orly-table id="userTable" loaddataoncreate includefilter maxrows="10" tabletitle="Search Results" class="invisible" data='${userListJson}'> --%>
	<orly-column field="action" label="Action" class="">
		<div slot="cell">
			<a href="\${`<c:url value='/users/userPage?id=\${model.userId}' />`}">
				<orly-icon id="\${`e_\${model.userId}`}" class="pointer" color="cornflowerblue" name='edit'></orly-icon>
			</a>
			<orly-icon id="\${`d_\${model.userId}`}" class="pointer" color="red" name='x'></orly-icon>
	    </div>
	</orly-column>
	<orly-column field="userId" label="User Id" class="" sorttype="natural"></orly-column>
	<orly-column field="firstName" label="First Name">
		<div slot="cell">
			<%-- Example 1 --%>
			<%-- Build the link using JSTL's c:url tag --%>
			<a href="\${`<c:url value='users?id=\${model.userId}' />`}">\${model.firstName}</a>
			
			<%-- Example 2 --%>
			<%-- Build the link using JSTL's contextPath --%>
			<%-- <a href="\${`${pageContext.request.contextPath}/users?id=\${model.userId}`}">\${model.firstName}</a> --%>
			
			<%-- Example 3 --%>
			<%-- Build the link using a relative path (notice "/" is missing before "users?") --%>
			<%-- <a href="\${`users?id=\${model.userId}`}">\${model.firstName}</a> --%>
		</div>
	</orly-column>
	<orly-column field="lastName" label="Last Name"></orly-column>
	<!-- <orly-column field="birthDate" label="Birth Date"></orly-column> -->
	<orly-column field="birthDateFormatted" label="Birth Date"></orly-column>
	<orly-column field="department" label="Department"></orly-column>
</orly-table>


<script>
	var debug = 1;
    var jsonString = "";
	var jsonUserArray;
	var userTable;
	var contextPath = '${pageContext.request.contextPath}';
	
	orly.ready.then(() => {
		// Get the JSON from the model and then create the JSON Object
		jsonString = '${userListJson}';
		jsonUserArray = JSON.parse(jsonString);
		
		// Initialize the table element variable
		userTable = orly.qid("userTable");
		
		// Populate the table via Cell Renderers
		//populateTable();		
		//printUsers();
		
		displayWelcomeMessage();
		addEventDelegation();
	});
	
	function addEventDelegation() {
		userTable.addEventListener("click", function(e) {
			try {
				let id = e.target.id;
				
				if (id != null && id != "undefined" && id.startsWith("d_")) {					
					id = id.substring(2,id.length);
					console.log("id="+id);
					
					let User = {};
					User.userId = id;
					
					// AJAX call to delete id
					fetch("<c:url value='/users/deleteUser' />", {
				        method: "POST",
				        body: JSON.stringify(User),
				        headers: {
				            "Content-Type": "application/json"
				        }
					}).then(function(response) {
					  	if (response.ok) {
					  		let jsonPromise = response.json(); // object from controller
					  		return jsonPromise;
					  	} else {
					  		throw new Error(getError(response));
					  	}
					}).then(function(Response) {						
						if (Response != null && Response != "undefined") {
							let messageType = "info";
							let message = Response.message;
							messageType = ((Response.messageType.length > 0) ? Response.messageType : messageType); 
							orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
							populateTableWithResponse(Response);
						} else {
							orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
						}
					}).catch(function(error) {
						console.log('There was a problem with your fetch operation: ', error.message);
					});
				}
			} catch (err) {
				console.log("Runtime Error-> " + err);
			} 
		});
	}
	
	function populateTableWithResponse(Response) {
		try {
			let userList = Response.userList;
			
			if (userList != null && userList != "undefined") {
				userTable.data = userList;
				gul = userList;
			} else {
				console.log("Unable to populate table with new user list");
			}	
		} catch (e) {
			console.log("Unable to populate table with new user list");	
		}
	}
	
	function displayWelcomeMessage() {
		try {			
			let data = {};
			data.message = "Hello World";
			
			// Make AJAX call to the server along with a message
			fetch("<c:url value='/users/getWelcomeMessage' />", {
			        method: "POST",
			        body: JSON.stringify(data),
			        headers: {
			            "Content-Type": "application/json"
			        }
			}).then(function(response) {
				// Get the response, grab the JSON, and return response so we
				// can process it in the next "then" (since this is a Promise...)
			  	if (response.ok) {
			  		console.log("response.status=", response.status);
			  		let message = response.json();
			  		console.log("message = " + message);
			  		return message
			  	}
			}).then(function(response) {
				let Message = JSON.parse(response);
			  	console.log("Message.message = " + Message.message);
			  	orly.qid("alerts").createAlert({type:"info", duration:"3000", msg:Message.message});
			    /* orly.qid("alerts").createAlert({duration:"3000", msg:Message.message}); */
			    /* orly.qid("myAlertManager").createAlert({type:typeStr, duration:"3000", msg:Message.message}); */
			}).catch(function(error) {
				console.log('There was a problem with your fetch operation: ', error.message);
			});
	
		} catch (err) {
			console.log("Error: " + err);
		}
	}
	
	function populateTable() {
		// Bad Form - do not do this
		//userTable.data = ${userListJson};
		
		// Proper Way - Set cell data using the json object
		userTable.data = jsonUserArray;
		
		// Update the column "First Name" using a Cell Renderer (link to userPage)
		userTable.updateColumn("firstName", "cellFn", function(row, cell, item) {
			return orly.wire()`<a href=\${`\${contextPath}/users?id=\${item.userId}`}>\${item.firstName}</a>`;
		});
		
  	    // Update the column "Birth Date" using a Cell Renderer to format the timestamp
  		userTable.updateColumn("birthDate", "cellFn", function(row, cell, item) {
  			// The usual way to format data using JSTL will not work since JSTL (Java) 
  			// is processed at page load. We need to make use of orly.formatDate 
  			//<fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy HH:mm"/>
  			
  			// Use the O'Reilly formatDate call
  			return orly.formatDate(new Date(item.birthDate), 'MM/DD/YYYY');
		});
	}
	
	function printUsers() {
		if (debug > 0) {
			for (let user of jsonUserArray) {
				console.log(user.firstName);
			}	
		}
	}
	
	function getError(errorObject) {
		if (errorObject == null || errorObject == "undefined") {
			return "Unknown Exception";
		} else {
			let status = errorObject.status;
			let text = errorObject.statusText;
			console.log(status + " " + text);
			return "Server Error. Status="+status+" Msg="+text;
		}
	}
</script>

-- DROP TABLE userroles;
-- DROP TABLE users;

CREATE TABLE users (
  user_id INTEGER GENERATED ALWAYS AS IDENTITY,
  first_name VARCHAR(64) NOT NULL,
  last_name VARCHAR(64) NOT NULL,
  department VARCHAR(64) NOT NULL,
  birth_date TIMESTAMP,
  PRIMARY KEY(user_id)
);

INSERT INTO users (first_name, last_name, department, birth_date)
VALUES
 ('Bob', 'Numbers', 'Accounting', '1982-01-24 00:00:00')
,('Anne', 'Calculateher', 'Accounting', '1985-04-28 00:00:00') 
,('Gibit', 'Toomenow', 'Collections', '1989-02-14 00:00:00') 
,('Aye', 'Takeit', 'Collections', '1982-01-24 00:00:00') 
,('Dee', 'Velop', 'Information Technology', '1976-03-26 00:00:00') 
,('Prog', 'Amer', 'Information Technology', '1977-05-02 00:00:00') 
;
    
CREATE TABLE userroles (
  id INTEGER GENERATED ALWAYS AS IDENTITY,
  user_id INTEGER NOT NULL,
  role VARCHAR(256) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO userroles (user_id, role) 
VALUES 
 (1,'User'), (1,'Finance'), (1, 'Accounting')
,(2,'User'), (2,'Finance'), (2, 'Accounting')
,(3,'User'), (3,'Finance'), (3, 'Accounting'), (3, 'Collections')
,(4,'User'), (4,'Finance'), (4, 'Accounting'), (4, 'Collections')
,(5,'User'), (5,'Development'), (5, 'IT'), (5, 'IT Manager')
,(6,'User'), (5,'Development'), (5, 'IT')
;


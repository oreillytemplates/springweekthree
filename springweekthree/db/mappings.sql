-- DROP TABLE order_items;
-- DROP TABLE orders;
-- DROP TABLE students;
-- DROP TABLE schools;
-- DROP TABLE facilities;
-- DROP TABLE teammembers;
-- DROP TABLE planners_projects;
-- DROP TABLE planners;
-- DROP TABLE projects;
-- DROP TABLE badges;
-- DROP TABLE interns;


-- ------ ORDERS -----------------

CREATE TABLE orders (
	order_id INTEGER GENERATED ALWAYS AS IDENTITY,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	PRIMARY KEY(order_id)
);

CREATE TABLE order_items (
	item_id INTEGER GENERATED ALWAYS AS IDENTITY,
	order_id INTEGER NOT NULL,
	sku INTEGER NOT NULL,
	PRIMARY KEY(item_id)
);

INSERT INTO orders (first_name, last_name)
VALUES ('Bob', 'Smith'),('Sue', 'Smith');

INSERT INTO order_items (order_id, sku)
VALUES (1, 153), (1, 629), (2, 496), (2, 351);

-- ------ SCHOOLS -----------------

CREATE TABLE schools (
	school_id INTEGER GENERATED ALWAYS AS IDENTITY,
	school_name VARCHAR(64),
	PRIMARY KEY(school_id)
);

INSERT INTO schools (school_name)
VALUES ('WKU'),('MSU');

CREATE TABLE students (
	student_id INTEGER GENERATED ALWAYS AS IDENTITY,
	school_id INTEGER,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	PRIMARY KEY(student_id)
);

INSERT INTO students (school_id, first_name, last_name)
VALUES 
 (1, 'Bob', 'Smith')
,(1, 'Sue', 'Smith')
,(2, 'Tim', 'Smith')
,(2, 'Kim', 'Smith');

-- ------ FACILITIES -----------------

CREATE TABLE facilities (
	facility_id INTEGER GENERATED ALWAYS AS IDENTITY,
	facility_name VARCHAR(64),
	PRIMARY KEY(facility_id)
);

INSERT INTO facilities (facility_name)
VALUES 
 ('Reasearch And Development')
,('Information Technology');

CREATE TABLE teammembers (
	teammember_id INTEGER GENERATED ALWAYS AS IDENTITY,
	facility_id INTEGER,
	first_name VARCHAR(64),
	last_name VARCHAR(64),
	PRIMARY KEY(teammember_id)
);

INSERT INTO teammembers(facility_id, first_name, last_name)
VALUES 
 (1, 'Bob', 'Smith')
,(1, 'Sue', 'Smith')
,(2, 'Tim', 'Smith')
,(2, 'Kim', 'Smith');


-- ------ PROJECTS -----------------

CREATE TABLE planners (
  planner_id INTEGER GENERATED ALWAYS AS IDENTITY,
  first_name VARCHAR(64) DEFAULT NULL,
  last_name VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY (planner_id)
);
 
INSERT INTO planners (first_name, last_name)
VALUES 
 ('Bob', 'Smith')
,('Sue', 'Smith')
,('Tim', 'Smith')
,('Kim', 'Smith');

CREATE TABLE projects (
  project_id INTEGER GENERATED ALWAYS AS IDENTITY,
  title VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY (project_id)
);

INSERT INTO projects(title)
VALUES 
 ('Wedding 1')
,('Wedding 2');
 
CREATE TABLE planners_projects (
  planner_id INTEGER NOT NULL,
  project_id INTEGER NOT NULL,
  PRIMARY KEY (planner_id,project_id),
  CONSTRAINT planner_project_ibfk_1 
   FOREIGN KEY (planner_id) REFERENCES planners (planner_id),
  CONSTRAINT planner_project_ibfk_2 
   FOREIGN KEY (project_id) REFERENCES projects (project_id)
);

INSERT INTO planners_projects(planner_id, project_id)
VALUES 
 (1,1)  /* Bob - Wedding 1 */
,(2,1)  /* Sue - Wedding 1 */
,(3,1)  /* Tim - Wedding 1 */
,(1,2)  /* Bob - Wedding 2 */
,(3,2)  /* Tim - Wedding 2 */ /* Kim - Wedding 2 */
,(4,2); 


-- ------ INTERNS -----------------

CREATE TABLE interns (
  intern_id INTEGER GENERATED ALWAYS AS IDENTITY,
  intern_name VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY (intern_id)
);

INSERT INTO interns(intern_name)
VALUES 
 ('Joe')
,('Larry')
,('Q');

CREATE TABLE badges (
  badge_id INTEGER GENERATED ALWAYS AS IDENTITY,
  intern_id INTEGER,
  issuer INTEGER DEFAULT NULL,
  PRIMARY KEY (badge_id)
);

INSERT INTO badges(intern_id, issuer)
VALUES 
 (1, 1)
,(2, 1)
,(3, 1);









